package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import org.openqa.selenium.By;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.Random;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.driver;
import static com.codeborne.selenide.WebDriverRunner.url;

public class LoginPage {

    String LOGIN = "login";
    String PASSWORD = "password";
    String LOGIN_BUTTON = ".v-btn__content";
    String ERROR_MESSAGE_TEXT = ".red--text";
    String CHANGE_PASSWORD_BUTTON = ".v-btn__content";

    @Step("Ввести неправильный логопасс {0},{1}")
    public void inputWrongLogoPass(String login, String password) {
        $(By.id(LOGIN)).sendKeys(login);
        $(By.id(PASSWORD)).sendKeys(password);
    }

    @Step("Ввести логопасс пользователя, незареганного на платформе {0},{1}")
    public void inputLogoPassNoExistUser(String login, String password) {
        $(By.id(LOGIN)).sendKeys(login);
        $(By.id(PASSWORD)).sendKeys(password);
    }

    @Step("Ввести логопасс пользователя {0}")
    public void inputLogoPasstUser(String login) {
        $(By.id(LOGIN)).sendKeys(login);
        $(By.id(PASSWORD)).sendKeys("KuTKpQszS2iSjug");
    }

    @Step("Кликнуть на кнопку \"Вход в систему\"")
    public void clickBtnEnterToSystem() {
        $(LOGIN_BUTTON).click();
    }

    @Step("Появилось сообщение об ошибке")
    public void errorMessageWrongLogoPass() {
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        $(ERROR_MESSAGE_TEXT).shouldHave(Condition.exactText("Неправильная пара логин/пароль"));
    }

    @Step("Пользователь остался на странице авторизации")
    public void userStayOnAuthorizationPage() {
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        Assert.assertEquals(url().toString(),
                "https://cabinet.huntica.works/login?redirect=%2F%3F");
    }

    @Step("Кликнуть на кнопку \"Восстановить пароль\"")
    public void clickRecoveryPasswordBtn() {
       $(By.xpath("//a[contains(text(),'Восстановить пароль')]")).click();
    }

    @Step("Ввести email")
    public void inputExistUserEmail(String email) {
        $(By.id(LOGIN)).sendKeys(email);
    }

    @Step("Ввести email пользователя, незарегистрированного в системе {0}")
    public void inputNotExistUserEmail(String email) {
        $(By.id(LOGIN)).sendKeys(email);
    }

    @Step("Кликнуть на кнопку \"Сменить пароль\"")
    public void clickChangePasswordBtn() {
        $(By.cssSelector(CHANGE_PASSWORD_BUTTON)).click();
    }

    @Step("Появилось сообщение об ошибке")
    public void errorMessageWrongEmail() {
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        $(By.cssSelector(ERROR_MESSAGE_TEXT))
                .shouldHave(Condition.text("Пользователь с такими e-mail/телефоном не найден"));
    }

    @Step("Появилось сообщение об отправке пароля")
    public void passwordSentMessage(){
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        $(By.cssSelector("div.vts__snackbar__message"))
                .shouldHave(Condition.text("Информация для восстановления отправлена на электронную почту."));
    }

    @Step("Произошел редирект на страницу авторизации")
    public void redirectOnLoginPage() {
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        Assert.assertEquals(url().toString(),
                "https://cabinet.huntica.works/login");
    }

    @Step("Нажать кнопку \"Назад\"")
    public void goBack() {
        driver().getWebDriver().navigate().back();
    }

    @Step("Обновить страницу")
    public void refreshPage() {
        driver().getWebDriver().navigate().refresh();
    }

    @Step("Пользователь находится на странице авторизации")
    public void userOnOnLoginPage() {
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        Assert.assertEquals(url().toString(),
                "https://cabinet.huntica.works/logout");
    }

}
