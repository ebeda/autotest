package pages;

import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selenide.$;

public class MenuBlockPage {

    String PANEL_BUTTON = "//div[contains(text(),'Панель')]";
    String ABOUT_COMPANY_BUTTON = "//div[contains(text(),'О компании')]";
    String VACANCIES_BUTTON = "//div[contains(text(),'Вакансии')]";
    String ACCOUNT_BUTTON = "//div[contains(text(),'Аккаунт')]";
    String LOGOUT_BUTTON = "//div[contains(text(),'Выйти')]";

    @Step("Нажать на кнопку \"Панель\"")
    public void clickOnPanelButton() {
        $(By.xpath(PANEL_BUTTON)).click();
    };

    @Step("Нажать на кнопку \"О компании\"")
    public void clickOnAboutCompanyButton() {
        $(By.xpath(ABOUT_COMPANY_BUTTON)).click();
    };

    @Step("Нажать на кнопку \"Вакансии\"")
    public void clickOnVacanciesButton() {
        $(By.xpath(VACANCIES_BUTTON)).click();
    };

    @Step("Нажать на кнопку \"Аккаунт\"")
    public void clickOnAccountButton() {
        $(By.xpath(ACCOUNT_BUTTON)).click();
    };

    @Step("Нажать на кнопку \"Выйти\"")
    public void clickOnLogoutButton() {
        $(By.xpath(LOGOUT_BUTTON)).click();
    };
}
