package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.testng.SoftAsserts;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import ru.yandex.qatools.allure.annotations.Step;
import utils.RandomGenerator;

import java.util.Random;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;

public class LendingPage {

    String LOGOUT = "//div[contains(text(),'Выйти')]";
    String RIGH_SWIPE_SLIDER = ".arrow.right.d-none.d-sm-block";
    String OFFER_CANDIDATE_SLIDE = "//div[contains(text(),'Приглашайте кандидатов')]";
    String TEST_INTERVIEW_LINK_ON_TOP = "//span[contains(text(),'Tестовое интервью')]";
    String TEST_INTERVIEW_LINK_IN_THE_MIDDLE = "//a[contains(text(),'тестовое собеседование')]";
    String REGISTRATION_BUTTON_ON_TOP = ".justify-center > div > div > button > span";
    String USER_DATA_FORM = "//div[contains(text(),'Пользователь')]";
    String START_TO_WORK_BUTTON = "//span[contains(text(),'Начать работу с Huntica')]";

    //Форма регистрации
    String COMPANY_NAME = "//input[@id='organization']";
    String COUNTRY = "//input[@id='input-143']";
    String USER_NAME = "//input[@id='firstName']";
    String USER_SURNAME = "//input[@id='lastName']";
    String EMAIL = "//input[@id='email']";
    String PHONE = "//input[@id='input-157']";
    String PASSWORD = "//input[@id='input-162']";
    String CONFIRM_PASSWORD = "//input[@id='input-165']";
    String AGREEMENT_CHECK_BOX = "//input[@id='input-168']/../div[contains(@class,'v-input--selection-controls__ripple')]";
    String REGISTER_BUTTON = "//*[@id='register-form']//button[contains(@class,'v-btn')]";
    String ERROR_MESSAGE_FIELDS = "//div[@class='v-messages__message']";
    String ERROR_MESSAGE_TEXT = "Поле обязательно для заполнения.";

    VacanciesPage vacanciesPage = new VacanciesPage();
    RandomGenerator randomGenerator = new RandomGenerator();

    Random random = new Random();

    @Step("Открыть сайт Huntica.works")
    public void openLendingPage(){
        open("https://huntica.works");
        Configuration.startMaximized = true;
     //   JavascriptExecutor jse = (JavascriptExecutor) getWebDriver();
    //    jse.executeScript("window.open('https://huntica:IngahBe2ah@huntica.online')");
    //    switchTo().window(0).close();
    }

    @Step("Кликнуть на кнопку \"Войти\"")
    public void clickBtnEnter(){
        $(".landing-header__login > span").click();
    }

    @Step("Пользователь видит меню ЛК")
    public void menuListExist() {
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        $(By.xpath(LOGOUT)).shouldHave(Condition.exactText("Выйти"));
        $(By.xpath("//div[contains(text(),'Панель')]")).shouldHave(Condition.exactText("Панель"));
    }

    @Step("Пролистать слайдер до слайда \"Приглашайте кандидатов\"")
    public void checkSlider() {
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        $(RIGH_SWIPE_SLIDER).doubleClick();
        $(By.xpath(OFFER_CANDIDATE_SLIDE)).shouldHave(Condition.exactText(" Приглашайте кандидатов "));
    }

    @Step("Кликнуть по кнопке \"Tестовое интервью\" в верхней части страницы")
    public void clickOnTestInterviewButtonOnTop() {
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        $(By.xpath(TEST_INTERVIEW_LINK_ON_TOP)).click();
        $(By.xpath(vacanciesPage.VACANCIES_LIST_LINK))
                .waitUntil(Condition.visible,3000)
                .shouldBe(Condition.visible);
    }

    @Step("Кликнуть по ссылке \"тестовое собеседование\" в середине страницы")
    public void clickOnTestInterviewLinkInTheMiddlOfPage() {
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        $(By.xpath(TEST_INTERVIEW_LINK_IN_THE_MIDDLE)).click();
        $(By.xpath(vacanciesPage.VACANCIES_LIST_LINK))
                .waitUntil(Condition.visible,3000)
                .shouldBe(Condition.visible);
    }

    @Step("Кликнуть по кнопке \"Зарегистрироваться\" в верхней части страницы")
    public void clickOnRegistrationButtonOnTop() {
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        $(REGISTRATION_BUTTON_ON_TOP).click();
        $(By.xpath(USER_DATA_FORM)).scrollIntoView(false)
                .waitUntil(Condition.visible,3000)
                .shouldBe(Condition.visible);
    }

    @Step("Кликнуть по кнопке \"Начать работу с Huntica\" в середине страницы")
    public void clickOnWorkWithHunticaButton() {
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        $(By.xpath(START_TO_WORK_BUTTON)).click();
        $(By.xpath(USER_DATA_FORM)).scrollIntoView(false)
                .waitUntil(Condition.visible,3000)
                .shouldBe(Condition.visible);
    }

    @Step("Ввести название компании")
    public void inputCompanyName() {
        String companyName = "Рога и копыта";
        $(By.xpath(COMPANY_NAME)).sendKeys(companyName);
    }

    @Step("Ввести имя пользователя")
    public void inputUserName() {
        String userName = "Рога и копыта";
        $(By.xpath(USER_NAME)).sendKeys(userName);
    }

    @Step("Ввести фамилию пользователя")
    public void inputUserSurame() {
        String userSurname = "Вася";
        $(By.xpath(USER_SURNAME)).sendKeys(userSurname);
    }

    @Step("Ввести email пользователя")
    public void inputUserEmail() {
        $(By.xpath(USER_SURNAME)).sendKeys(randomGenerator.emailGenerator());
    }

    @Step("Проверить, что кнопка \"Зарегистрироваться\" некликабельна")
    public void verifyRegisterButtonNotClickable() {
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        $(By.xpath(REGISTER_BUTTON)).shouldBe(Condition.disabled);
    }

    @Step("Принять пользовательское соглашение")
    public void checkUserAgreement() {
        $(By.xpath(AGREEMENT_CHECK_BOX)).click();
    }

    @Step("Проверить сообщения об ошибках, если пол не заполнены")
    public void checkErrorsIfFieldsAreEmpty() {
        Configuration.assertionMode = Configuration.assertionMode.SOFT;
        $$(By.xpath(ERROR_MESSAGE_FIELDS)).shouldHaveSize(8);
        $$(By.xpath(ERROR_MESSAGE_FIELDS)).first().shouldHave(Condition.text(ERROR_MESSAGE_TEXT));
    }

    @Step("Кликнуть на кнопку \"Зарегистрироваться\"")
    public void clickOnRegistrationButtonOnBottom() {
        $(By.xpath(REGISTER_BUTTON)).click();
    }
}
