package autotests;

import com.codeborne.selenide.testng.SoftAsserts;
import io.qameta.allure.TmsLink;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.LendingPage;
import pages.LoginPage;
import pages.MenuBlockPage;
import pages.PanelPage;
import ru.yandex.qatools.allure.annotations.Description;
import utils.DriverSettings;

import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

@Listeners({SoftAsserts.class})

public class PersonalOfficeTest {

    @BeforeMethod
    private void setUpDriver() {
    }

    @AfterMethod
    private void closeDriver() {
        getWebDriver().quit();
    }

    String existUser = "ms@huntica.works";
    String notExistUser = "test@test.test";

    LendingPage lendingPage = new LendingPage();
    LoginPage loginPage = new LoginPage();
    MenuBlockPage menuBlockPage = new MenuBlockPage();
    PanelPage panelPage = new PanelPage();
    DriverSettings driverSettings = new DriverSettings();

    @Test
    @TmsLink("LK-40")
    @Description("Авторизация c неправильным логопассом")
    public void authrizedWithWrongLogopass(){
     lendingPage.openLendingPage();
     sleep(5000);
     lendingPage.clickBtnEnter();
     loginPage.inputWrongLogoPass(existUser,"admin123");
     loginPage.clickBtnEnterToSystem();
     loginPage.errorMessageWrongLogoPass();
     loginPage.userStayOnAuthorizationPage();
    }

    @Test
    @TmsLink(value = "LK-45")
    @Description(value = "Авторизация пользователя, незарегистрированного в системе")
    public void authrizedNoExistUser(){
        lendingPage.openLendingPage();
        sleep(5000);
        lendingPage.clickBtnEnter();
        loginPage.inputLogoPassNoExistUser(notExistUser,"admin123");
        loginPage.clickBtnEnterToSystem();
        loginPage.errorMessageWrongLogoPass();
        loginPage.userStayOnAuthorizationPage();
    }

    @Test
    @TmsLink(value = "LK-39")
    @Description(value = "Авторизация с корректным логопассом")
    public void authrizedUser(){
        lendingPage.openLendingPage();
        sleep(5000);
        lendingPage.clickBtnEnter();
        loginPage.inputLogoPasstUser(existUser);
        loginPage.clickBtnEnterToSystem();
        lendingPage.menuListExist();
    }

    @Test
    @TmsLink(value = "LK-42")
    @Description(value = "Восстановление пароля у незарегистрированного пользователя")
    public void recoveryPasswordForNotExistUser(){
        lendingPage.openLendingPage();
        sleep(5000);
        lendingPage.clickBtnEnter();
        loginPage.clickRecoveryPasswordBtn();
        loginPage.inputNotExistUserEmail(notExistUser);
        loginPage.clickChangePasswordBtn();
        loginPage.errorMessageWrongEmail();
    }

    @Test
    @TmsLink(value = "LK-41")
    @Description(value = "Восстановление пароля у зарегистрированного пользователя")
    public void recoveryPasswordForExistUser(){
        lendingPage.openLendingPage();
        sleep(5000);
        lendingPage.clickBtnEnter();
        loginPage.clickRecoveryPasswordBtn();
        loginPage.inputExistUserEmail("sofos99361@grokleft.com");
        loginPage.clickChangePasswordBtn();
        loginPage.passwordSentMessage();
        loginPage.redirectOnLoginPage();
    }

    @Test
    @TmsLink(value = "LK-44")
    @Description(value = "Выход из ЛК")
    public void logout(){
        lendingPage.openLendingPage();
        sleep(5000);
        lendingPage.clickBtnEnter();
        loginPage.inputLogoPasstUser(existUser);
        loginPage.clickBtnEnterToSystem();
        lendingPage.menuListExist();
        menuBlockPage.clickOnLogoutButton();
        loginPage.goBack();
        loginPage.redirectOnLoginPage();
        loginPage.refreshPage();
        loginPage.redirectOnLoginPage();
    }
}
