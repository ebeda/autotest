package autotests;

import com.codeborne.selenide.testng.SoftAsserts;
import io.qameta.allure.TmsLink;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.*;
import ru.yandex.qatools.allure.annotations.Description;
import utils.DriverSettings;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;


@Listeners({SoftAsserts.class})

public class LendingPageTest {

    @BeforeMethod
    private void setUpDriver() {
        open("https://huntica.works/");
        getWebDriver().manage().window().maximize();
    }

    @AfterMethod
    private void closeDriver() {
        getWebDriver().quit();
    }

    LendingPage lendingPage = new LendingPage();
    LoginPage loginPage = new LoginPage();
    DriverSettings driverSettings = new DriverSettings();

    @Test
    @Description("Полистать слайдер")
    public void swipeSlider() {
        lendingPage.checkSlider();
    }

    @Test
    @Description("Проверить, что ссылки на тестовое интервью кликабельны и ведут на нужный URL")
    public void verifyTestInterviewLinkClickable() {
        lendingPage.clickOnTestInterviewButtonOnTop();
        driverSettings.goBack();
        lendingPage.clickOnTestInterviewLinkInTheMiddlOfPage();
    }

    @Test
    @Description("Проверить, что работает скорлл на форму регистрации")
    public void verifyScrollOnRegistrationForm() {
        lendingPage.clickOnRegistrationButtonOnTop();
        lendingPage.clickOnWorkWithHunticaButton();
    }

    @Test
    @Description("Проверить текст об ошибках, когда форма регистрации не заполнена")
    public void verifyErrorMessageIfUserNotFillFields() {
        lendingPage.verifyRegisterButtonNotClickable();
        lendingPage.checkUserAgreement();
        lendingPage.clickOnRegistrationButtonOnBottom();
        lendingPage.checkErrorsIfFieldsAreEmpty();
    }
}
