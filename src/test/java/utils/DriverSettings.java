package utils;

import com.codeborne.selenide.WebDriverRunner;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;

public class DriverSettings {

    public void goBack() {
        WebDriverRunner.getWebDriver().navigate().back();
    }
}
